# Need to get the modules matplotlib, numpy, scipy, emcee and corner to run this script.
# On linux: apt get install x (x= matplotlib, numpy, scipy, pip)
# pip install x (emcee, corner)
# On Mac: brew install x (x=matplotlib,numpy,scipy,pip) and follow with pip similarly to linux users.

# Source code written by Julien Billard.
# Modification by Noemie Bastidon, 24 Apr 2017.

import matplotlib.pyplot as plt
import numpy as np
import emcee
import corner

import scipy.optimize as op
from scipy.optimize import root as racine
from scipy.optimize import root
from scipy.optimize import minimize, rosen, rosen_der
from pylab import *
from math import *
from matplotlib import pyplot
from numpy import linalg as LA
from numpy.linalg import inv
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import MaxNLocator


###### INPUT DATA AND PARAMETERS ######

# Different temperature of the bath.
Tb = np.array([18e-3, 20e-3, 22e-3, 25e-3, 30E-3])

# V data for different bath temperature (NTD).
V16data=np.array([0.000102694, 0.000195114, 0.000402626, 0.000958653, 0.001890457, 0.003522283, 0.006833836, 0.009339155, 0.010849132, 0.011207009, 0.010738653, 0.009796712])
V18data=np.array([5.50913E-05, 0.000115091, 0.000223973, 0.000559543, 0.001114384, 0.002151963, 0.00470589, 0.007181142, 0.00918089, 0.010291804, 0.010204954, 0.009553973])
V20data=np.array([2.78311E-05, 5.45662E-05, 0.000112374, 0.000275868, 0.000553014, 0.001090046, 0.002592854, 0.004544155, 0.006804795, 0.008806005, 0.009303219, 0.009119201])
V22data=np.array([1.38128E-05, 2.85616E-05, 5.64384E-05, 0.000140274, 0.000279498, 0.00055774, 0.001366438, 0.002594247, 0.004476027, 0.00702411, 0.008140183, 0.008531804])
V25data=np.array([5.47945E-06, 1.09361E-05, 2.20548E-05, 5.48402E-05, 0.000108676, 0.000219612, 0.000546005, 0.001077671, 0.002067991, 0.00424258, 0.005964452, 0.007322763])
V30data=np.array([1.59817E-06, 2.96804E-06, 5.84475E-06, 1.49087E-05, 2.96119E-05, 5.92009E-05, 0.000147717, 0.000294772, 0.000587215, 0.001423151, 0.002613493, 0.004671849])

# I polarization data for NTD.
Ipdata=np.array([5E-12, 1E-11, 2E-11, 5E-11, 1E-10, 2E-10, 5E-10, 0.000000001, 0.000000002, 0.000000005, 0.00000001, 0.000000025])

Vdata = [V18data,V20data,V22data,V25data,V30data]

EventEnergy = 1460. # keV
Gain = 2398.

# Input file with (V,t) for one pulse, read it and close it.
f = open('RED1_BulkEvent_1nA.txt', 'r')
# Create dt_pulse variable for time info and Vdata_pulse_1 for voltage info.
dt_pulse, Vdata_pulse_1 = [],[]
# Loop over lines and extract variables of interest
for line in f:
    line = line.strip()
    columns = line.split()
    dt_pulse.append(float(columns[0]))
    Vdata_pulse_1.append(float(columns[1])/EventEnergy/Gain)
f.close()


Vdata_pulse_1 = np.asarray(Vdata_pulse_1)

Vdata_pulse = [Vdata_pulse_1] # Voltage data measured with a RED1 detector (extracted from .txt)
Tb_pulse_data = 18e-3 # Tb = Temperature of the bath of 18 mK
Ip_data_pulse = np.array([1e-9]) # Polarization current


Vge = 48 #cm3 Germanium crystal volume.
nk= 4 # Kapitza exponent (used to compute power between Ge crystal and bath).
n = 6 # Exponent for NTD (used to compute power flowing through phonon and electron system of the NTD).
ng = 3.5 # Ask Julien, unknown exponent ?

Vntd = 15e-3 # cm^3 # Volume of the NTD
Sntd = 15 #mm2 # Surface of the NTD
Sau = 13 #mm2 # Surface of the gold pads


Energy = 1000*1.6E-19   # Deposited energy






###### COMPUTE EIGENVALUES OF THE MATRIX ######


# Non-linear steady state equation
def temp(Tb, Ip, R0, T0, gep,gk,n,nk):
    def tmp(x):
        f = [Ip**2*R0*exp(sqrt(T0/x[0])) - Vntd*gep*(x[0]**n - x[1]**n), Vntd*gep*(x[0]**n - x[1]**n) - Sau*gk*(x[1]**nk-Tb**nk)]
        #x0=Te, x1=Tp, x2=Ts
        return f
    return tmp
    

# Fill table with parameters.
def fill_tab(tb, R0, T0, gep,gk,n,nk):
    def get_sol(ip):
        sol=root(temp(tb, ip,R0, T0, gep,gk,n,nk), [tb,tb], jac=False, method='lm')
        r=R0*exp(sqrt(T0/sol.x[0]))
        a,b = sol.x
        c=b
        return a,b,c,r, r*ip
    
    list_of_5_tuple=map(get_sol, Ipdata)
    return map(np.array, zip(*list_of_5_tuple))
    
# Compute IV curve for given parameters.
def compute_IV(x):
    R0 = x[0]
    T0 = x[1]
    gep = x[2]
    gk = x[3]
    te, tp, ta, r, v = zip(*map(lambda t: fill_tab(t, R0,T0,gep,gk,n,nk), Tb))
    return v
    
# Compute single pulse for given parameters.
def ComputePulse(x, Ip_pulse, Tb_pulse, t, toffset, E):
    R0 = x[0]
    T0 = x[1]
    gep = x[2]
    gk = x[3]
    gglue = x[4]

    sol=root(temp(Tb_pulse, Ip_pulse,R0, T0, gep,gk,n,nk), [Tb_pulse,Tb_pulse], jac=False, method='lm')
    Te, Tp= sol.x
    Ta=Tp

    Ce = x[5]*Te*Vntd#1.1e-6*Te*Vntd  # Heat capacities
    Cp = x[6]*Tp**3*Vntd
    Ca = x[6]*Ta**3*Vge
    
    Gep1 = n*gep*Vntd*Te**(n-1)  # Conductivities
    Gep2 = n*gep*Vntd*Tp**(n-1)
    
    Gpb = nk*gk*Sau*Tp**(nk-1)
    
    Gpa1 = ng*gglue*Sntd*Tp**(ng-1)
    Gpa2 = ng*gglue*Sntd*Ta**(ng-1)
    
    R = R0*exp(sqrt(T0/Te)) # Resistance
    a = -0.5*sqrt(T0/Te)   # Correspond to alpha in the written note
    coef = a*Ip_pulse*R/Te # Coefficient used in the computation of the temperature perturbations

    # Declare matrix M
    M = np.array([[-a*Ip_pulse**2*R/(Te*Ce) + Gep1/Ce , -Gep2/Ce, 0],
                  [-Gep1/Cp, (Gep2+Gpa1+Gpb)/Cp, -Gpa2/Cp],
                  [0, -Gpa1/Ca, Gpa2/Ca]])
    
    Eig, P = LA.eig(M) # Compute eigenvalues and eigenvectors of M
    eps = 0.
    P_1 = inv(P) # Compute inverse of the eigenvectors
    phi0 = np.array([E*eps/Ce, 0, E*(1-eps)/Ca])  # Correspond to the F in the written document, here it is an event in the absorber.
    A = P_1.dot(phi0)
    

    deltaT_Mat = P.dot(A)
    tau = 1.0/Eig
    t = np.asarray(t)
    
    exp_vec = map(lambda x,y: y*np.exp(-t/x),tau,A)
    deltaT_exp = P.dot(exp_vec)

    dTe, dTp, dTa = map(np.array, deltaT_exp)
    dTe_off = np.zeros(len(dTe))
    dTe_sub = dTe[0:int(len(dTe)-toffset*1000)]
    dTe_off[t>=toffset]=dTe_sub
    Voltaget = -coef*dTe_off
    return Voltaget
    
# Run ComputePulse for different input parameters.
def ComputeMultiPulse(x, Ip_pulse, Tb_pulse, t, toffset, E):
    VoltagePulse = (map(lambda z: ComputePulse(x, z, Tb_pulse, t, toffset, E), Ip_pulse))
    return VoltagePulse
    
# Compute loglikelihood by comparing computed IV curves data to input data.
def loglikelihood(x):
    v = compute_IV(x)
    diff = zip(*map(lambda th,dt: ((th - dt)/(0.01*dt))**2, v, Vdata))
    loglikelihood = -0.5*sum(diff)
    return loglikelihood

# Compute loglikelihood by comparing computed single pulses data to input data.
def loglikelihood_Pulse(x):
    Vpulse = ComputeMultiPulse(x, Ip_data_pulse, Tb_pulse_data, dt_pulse, 0.25, Energy)
    diff = zip(*map(lambda th,dt: ((th - dt)/(0.05*dt))**2, Vpulse, Vdata_pulse))
    loglikelihood = -0.5*sum(diff)
    return loglikelihood
    

# Return 0 if the parameters R0, T0, gep, gk, gglue, Ce and Ca (defined in x0) are in the defined range of acceptance.
# Return (- infiny) if one of the parameters or more is/are out of bound.
def lnprior(theta):
    R0, T0, gep, gk, gglue, Ce, Ca = theta
    if 0.5 < R0 < 10 and 1. < T0 < 7. and 0. < gep < 150. and 0.5e-5 < gk < 50e-5 and 0.5e-5 < gglue < 50e-5 and 0.5e-6 < Ce < 50e-6 and 0.5e-6 < Ca < 50e-6:
         return 0.0
    return -np.inf
    
# Compute the total log likelihood by adding up result for IV curves and single pulses.
def lnlike(theta):
    loglike = loglikelihood(theta) + loglikelihood_Pulse(theta)
    return loglike


# If the parameters R0, T0, gep, gk, gglue, Ce and Ca (defined in x0) are in the defined range, will compute lnlike(x0) and return the result of the function.
def lnprob(theta):
    lp = lnprior(theta)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike(theta)

# Initial parameters
x0 = [1.0454535489001815, 4.7688199363593622, 55.462275395215855, 5.1230219725255889e-05, 0.00010166381833233266, 1.0103846566226746e-06, 2.6415398430849992e-06]

# Call every functions defined above in order to compute the total log likelihood.
nll = lambda *args: -lnprob(*args)

#Define bounds for optimization test.
bnds = [(0.5,10.), (1.,7.), (0.,150.), (0.5e-5,50e-5), (0.5e-5,50e-5), (0.5e-6,50e-6), (0.5e-6,50e-6)]

# Minimization routine of the log likelihood using the L-BFGS-B method.
# L-BFGS-B stands for Limited memory Broyden-Fletcher-Goldfarb-Shanno method with bounds constraints.
# This methods is used to look for extremum without computing the Hessian matrix.
# op.minimize(function, initial guess, bounds for each parameters, method, options)
# ftol >> Absolute error in function between iterations that is accepatable for convergence.
# disp >> Set to true to print convergence message.
result = op.minimize(nll, x0, bounds=bnds, method='L-BFGS-B', options={'ftol': 1e-10, 'disp': True})

# Return the 7 parameters.
print result.x

# Rerun a minimization routine of the log likelihood using similar method and using as initial guess the result of the first optimization result.
result2 = op.minimize(nll, result.x, bounds=bnds, method='L-BFGS-B', options={'ftol': 1e-10, 'disp': True})
print result2.x

# Print general result.
print(""" Minimizer result:
    Likelihood = {0}
    """ .format(lnprob(result2.x)))





###### RUN MCMC ######

# Sample the probability distribution obtained beforehand by using a Markov Chain Monte Carlo.

# Set up the sampler.
ndim, nwalkers = 7, 20
pos = [result2["x"] + 0.05*result2.x*np.random.randn(ndim) for i in range(nwalkers)]
sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob)
print pos
# Clear and run the production chain.
print("Running MCMC...")
nsteps = 2000
sampler.run_mcmc(pos, nsteps, rstate0=np.random.get_state())
print("Done.")




###### PLOT ######



# Return four plots:
# A plot to visualize the time series of the parameters in the chain.
# 2D diagrams showing the result of the MCMC.
# The input pulse in blue with the MCMC result in red (in blakc, the variance can be seen).
# IV curves for different temperatures. 


plt.clf()
fig, axes = plt.subplots(ndim, 1, sharex=True, figsize=(8, 9))
axes[0].plot(sampler.chain[:, :, 0].T, color="k", alpha=0.4)
axes[0].yaxis.set_major_locator(MaxNLocator(5))
axes[0].axhline(result2.x[0], color="#888888", lw=2)
axes[0].set_ylabel("$R0$")

axes[1].plot(sampler.chain[:, :, 1].T, color="k", alpha=0.4)
axes[1].yaxis.set_major_locator(MaxNLocator(5))
axes[1].axhline(result2.x[1], color="#888888", lw=2)
axes[1].set_ylabel("$T0$")

axes[2].plot(sampler.chain[:, :, 2].T, color="k", alpha=0.4)
axes[2].yaxis.set_major_locator(MaxNLocator(5))
axes[2].axhline(result2.x[2], color="#888888", lw=2)
axes[2].set_ylabel("$gep$")

axes[3].plot(sampler.chain[:, :, 3].T, color="k", alpha=0.4)
axes[3].yaxis.set_major_locator(MaxNLocator(5))
axes[3].axhline(result2.x[3], color="#888888", lw=2)
axes[3].set_ylabel("$gk$")

axes[4].plot(sampler.chain[:, :, 4].T, color="k", alpha=0.4)
axes[4].yaxis.set_major_locator(MaxNLocator(5))
axes[4].axhline(result2.x[4], color="#888888", lw=2)
axes[4].set_ylabel("$gglue$")

axes[5].plot(sampler.chain[:, :, 5].T, color="k", alpha=0.4)
axes[5].yaxis.set_major_locator(MaxNLocator(5))
axes[5].axhline(result2.x[5], color="#888888", lw=2)
axes[5].set_ylabel("$Ce$")

axes[6].plot(sampler.chain[:, :, 6].T, color="k", alpha=0.4)
axes[6].yaxis.set_major_locator(MaxNLocator(5))
axes[6].axhline(result2.x[6], color="#888888", lw=2)
axes[6].set_ylabel("$Ca$")
axes[6].set_xlabel("step number")


fig.tight_layout(h_pad=0.0)
fig.savefig("line-time.png")


burnin = 200
samples = sampler.chain[:, burnin:, :].reshape((-1, ndim))

fig = corner.corner(samples, bins=50, smooth=1, labels=["$R0$", "$T0$", "$gep$", "$gk$", "$gglue$", "$Ce$", "$Ca$"])
fig.savefig("line-triangle.png")

# Compute the quantiles.
R0_mcmc, T0_mcmc, gep_mcmc, gk_mcmc, gglue_mcmc, Ce_mcmc, Ca_mcmc = map(lambda z: (z[1], z[2]-z[1], z[1]-z[0]),
                             zip(*np.percentile(samples, [16, 50, 84],
                                                axis=0)))

resultMCMC = [R0_mcmc[0],T0_mcmc[0],gep_mcmc[0], gk_mcmc[0], gglue_mcmc[0], Ce_mcmc[0], Ca_mcmc[0]]

print(""" MCMC result:
            R0 = {0[0]} +{0[1]} -{0[2]}
            T0 = {1[0]} +{1[1]} -{1[2]}
            gep = {2[0]} +{2[1]} -{2[2]}
            gk = {3[0]} +{3[1]} -{3[2]}
            gglue = {4[0]} +{4[1]} -{4[2]}
            Ce = {5[0]} +{5[1]} -{5[2]}
            Ca = {6[0]} +{6[1]} -{6[2]}
            LogLike = {7}
""" .format(R0_mcmc, T0_mcmc, gep_mcmc, gk_mcmc, gglue_mcmc, Ce_mcmc, Ca_mcmc,lnprob(resultMCMC)))


vMCMC = compute_IV(resultMCMC)
Vpulse = ComputeMultiPulse(resultMCMC, Ip_data_pulse, Tb_pulse_data, dt_pulse, 0.25, Energy)


fig11 = plt.figure(11)
plt.ylabel('Voltage [V]')
plt.xlabel('Current [A]')
plt.yscale('log')
plt.xscale('log')

for R0, T0, gep, gk, gglue, Ce, Ca in samples[np.random.randint(len(samples), size=100)]:
    resultMCMC_step = [R0, T0, gep, gk, gglue, Ce, Ca]
    vMCMC_step = compute_IV(resultMCMC_step)
    map(lambda t: plt.plot(Ipdata, t, "-",color="k", alpha=0.4), vMCMC_step)

map(lambda t: plt.plot(Ipdata, t, ".",markersize=10), Vdata)
map(lambda t: plt.plot(Ipdata, t, "--",color="r", lw=1), vMCMC)
fig11.savefig("BestFit.png")


fig12 = plt.figure(12)
plt.ylabel('Voltage [V]')
plt.xlabel('Time [S]')


for R0, T0, gep, gk, gglue, Ce, Ca in samples[np.random.randint(len(samples), size=100)]:
    resultMCMC_step = [R0, T0, gep, gk, gglue, Ce, Ca]
    Vpulse_step = ComputeMultiPulse(resultMCMC_step, Ip_data_pulse, Tb_pulse_data, dt_pulse, 0.25, Energy)
    map(lambda t: plt.plot(dt_pulse, t, "-",color="k", alpha=0.4), Vpulse_step)

map(lambda t: plt.plot(dt_pulse, t, "-",lw=2), Vdata_pulse)
map(lambda t: plt.plot(dt_pulse, t, "--",color="r",lw=2), Vpulse)
fig12.savefig("Pulse.png")

plt.show()




